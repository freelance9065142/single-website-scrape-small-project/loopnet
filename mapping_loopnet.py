import json

import pandas as pd

clean_datas = []

with open(f'D:\\freelance\\restaurant-bussiness-scrapping\\loopnet\\link_loopnet.json', 'r') as file:
    json_file = json.load(file)
    for data in json_file:
        if data.get('value'):
            value = data['value']
            search = value['bfsSearchResult']
            raw_data = search.get('value')
            for data in raw_data:
                clean_data = {
                    'Title': data.get('heading'),
                    'Asking Price': data.get('askingPrice'),
                    'Gross Revenue': data.get('grossIncome'),
                    'Established Year': data.get('yearEstablished'),
                    'Cash Flow': data.get('cashflow'),
                    'Rent': data.get('leaseRatePerSquareFoot'),
                    'Listing ID': data.get('listNumber'),
                    'Ad Line': data.get('adLine'),
                    'Description': data.get('summary'),
                    'Location': data.get('locationDetail'),
                    'Inventory': 'Included in asking price' if data.get('inventoryIncludedInAskingPrice') else 'Not Included in asking price',
                    'Real Estate': 'Owned' if data.get('realEstate') else 'Leased',
                    'Employees': data.get('employees'),
                    'Building Square Feet': data.get('buildingSquareFeet'),
                    'Facilities': data.get('facilities'),
                    'Growth & Expansion': data.get('growth'),
                    'Support & Training': data.get('support'),
                    'Reason for Selling': data.get('reasonsForSelling'),
                    'Lease Expiration': data.get('leaseExpirationDate'),
                    'Financing': data.get('financingOptions'),
                    'Competition': data.get('competition'),
                    'Franchise': 'This business is an established franchise' if data.get('isFranchise') else 'This business is NOT an established franchise',
                    'URL': f'https://www.loopnet.com/biz{data.get("urlStub")}'
                }
                clean_datas.append(clean_data)
    df = pd.DataFrame(clean_datas)
    df.to_csv('D:\\freelance\\restaurant-bussiness-scrapping\\data.csv', index=False)
    print('done')
    