import requests
import json

cookies = {
    'LNUniqueVisitor': '66fa2657-e069-4581-806e-955af663489c',
    '_gcl_au': '1.1.353722489.1696919725',
    '_gid': 'GA1.2.474567762.1696919727',
    'visitor_id923983': '1179808731',
    'visitor_id923983-hash': '5c4bec8c28be7cac40155c5e07eee06f698962755bf8a186e3e73b62aafe97d6fe1b610b776e27c0613e14be2a50f67ab58d6ad8',
    'SessionFarm_GUID': 'f0c5e7a6-d120-4241-91e0-78641ff45512',
    '__RequestVerificationToken': 'HZ75vg0LBDCzlp9_DXSVgNsErPtgfgIsb7J_57YnoQMrLldUPPWcuJA6oxsLFoZTUENlAjnFtr9gBpyVFGiP_d6IqWQ1',
    'lnwbprddc': '04',
    'cr_gum2': 'C_DYOdCsyW9h7L3EbzB_iTtyp6Z25zDe',
    'g_state': '{"i_p":1697077953664,"i_l":2}',
    'LastSearchPropertyType': '%7B%22id%22%3A9001%2C%22pname%22%3A%22Office%22%2C%22lid%22%3Anull%2C%22pid%22%3A8%2C%22sid%22%3A0%2C%22scid%22%3A0%2C%22name%22%3A%22Office%22%2C%22namel%22%3A%22office%22%2C%22isforsale%22%3Afalse%2C%22isbbs%22%3Afalse%2C%22notfound%22%3Atrue%2C%22url%22%3Anull%7D',
    'LastSearchLocation': '%7b%22Typeahead%22%3a%22United+States%22%2c%22Geography%22%3a%7b%22ID%22%3a%221990%22%2c%22Display%22%3a%22United+States%22%2c%22GeographyType%22%3a15%2c%22Address%22%3a%7b%22CountryCode%22%3a%22US%22%2c%22ListingGeoCoded%22%3afalse%2c%22Country%22%3a%22US%22%2c%22CountryName%22%3a%22United+States%22%7d%2c%22Location%22%3a%7b%22y%22%3a45.176882%2c%22x%22%3a-123.2829875%7d%2c%22BoundingBox%22%3a%7b%22LowerRight%22%3a%7b%22y%22%3a18.9249%2c%22x%22%3a-66.940119%7d%2c%22UpperLeft%22%3a%7b%22y%22%3a71.428864%2c%22x%22%3a-179.625856%7d%7d%2c%22MatchType%22%3a15%2c%22SelectedCurrencyType%22%3a0%2c%22SelectedMeasurementConversion%22%3a0%2c%22DisplayCountryCode%22%3a%22USA%22%2c%22TypeaheadUIDisplay%22%3a%22United+States+-+USA%22%7d%2c%22Culture%22%3a%22en-US%22%7d',
    'MeasurementConversion': '2',
    'numberOfSearches': '2',
    'SearchesSinceLastIntercept': '2',
    'UOMFromSRP': '%7b%22LeaseRatePerSizeUom%22%3a0%2c%22UseDefaultLeaseRateUomAndTerm%22%3atrue%2c%22LeaseRateTerm%22%3a0%2c%22SpaceAvailableUom%22%3a2%2c%22BuildingSizeUom%22%3a1%2c%22LotSizeUom%22%3a1%7d',
    'cto_bundle': 'LlRTzF92WVVleU5MNkY2SEFCOERxeXBLZkdPT3dZdCUyQnU2dmhYNWdiSUVEYklLM3pJRzA0TlJDbnhlYWZka05qRTdMSUZGUGt1bldpN1dHY2M3QUZmU0pkN05LMEZJNG0lMkJzZU9Wc1RZS1p6aEc1MEFQbSUyRlZsYTlMeUFUNmtsaHJDMVdxYjRkWnNhTHJrZW1oeWN6ZGU2QWlSWFJ0MHNTRUVwMEp4UFFaaTYwQk52UFdqdnlzaFhKSnFMdTRGTkhLMXZkYjhZcHdLJTJGdTJkcjNKelduaTB6NHolMkZ1R0RsRFBiTWpjdkRwODlRemVzcHRWZ0cyZllNanp4Q1YwN1BjaEJxdE9uelhsTnRYcDVocFVvJTJGOUZSUTVGbE9oVW56U0hlYTNVQmRoQWRCeE4lMkJnbUs0QUhNM1BXanpsb3hUZ3J3S0dMZ2RK',
    'gip': '%7b%22Display%22%3a%22United+States%22%2c%22GeographyType%22%3a15%2c%22Address%22%3a%7b%22City%22%3a%22San+Francisco%22%2c%22PostalCode%22%3a%2294111%22%2c%22State%22%3a%22CA%22%2c%22CountryCode%22%3a%22US%22%2c%22Location%22%3a%7b%22y%22%3a37.7749295%2c%22x%22%3a-122.4194155%7d%2c%22ListingGeoCoded%22%3afalse%2c%22Country%22%3a%22US%22%7d%2c%22Location%22%3a%7b%22y%22%3a-6.13%2c%22x%22%3a106.75%7d%2c%22MatchType%22%3a0%2c%22SelectedCurrencyType%22%3a0%2c%22SelectedMeasurementConversion%22%3a0%2c%22lc%22%3a%22en-US%22%2c%22cy%22%3a%22USD%22%2c%22tgeo%22%3a%7b%22cc%22%3a%22ID%22%2c%22sc%22%3a%2204%22%2c%22ct%22%3a%22Jakarta%22%2c%22pc%22%3a%2211430%22%2c%22ly%22%3a-6.13%2c%22lx%22%3a106.75%2c%22lc%22%3a%22en-US%22%2c%22cy%22%3a%22USD%22%7d%7d',
    'bm_mi': '103395C52F5C47400BCF586D5A4FDFE2~YAAQnegyF0ElFBqLAQAA/chuHRXJ6B2yqjhj9n9WE1ggUS1/hstnwDIOY6f8oJ2RgN98Ae4MNcooNjvwfFZdHrkww6WQQBLDELV/ZVEkoezOGNflAAdAhr8WVGFrwgsVUWbmrIbHt62zyV7/DUvQMJobP/H8Oe+vOTT3v8zWrKFiFjy/bvtmN4JkBIOieqfVj/TQPmgQtIlUnYtzyAnN01T8a2PmpOS0t9uM/mgUCF0DUtFLpDXF+NOEN0TZaS2MeUfGDAWODuO5xI+FWFO8CLMzwa6kC+T59he7o9hNbdQxLFRM/q1fPs/A0OvZxH3vFnDBiRZttHKwq6OW2cOa1TxdWZ7i07Obk+Z/NbQ+Lbtrf8ndF/HdV4nPHA==~1',
    'bm_sv': 'A8495C66B5DB131BE201953AD2024F6B~YAAQnegyF58lFBqLAQAAIdJuHRXjwQ3MOQF0aKcEtaBixoATBo+W8K/fv/MjvUbxCGqm9HSx/gvMK3vxt6fB+h0jZm9ZuT2whLg8N3K0X2o4w1NBm+bxZqxS4z16L6OUWM95Bn8g9RNHjQqL+jP3qQTrcAEOs6tDyu4KVZBjyTow8r9S87I6dSS52Qc7egd7KRJ1SOF14uON506mb0fanthMBdet690CGKA/D2vjymZw3dUl9kwV8mP7L90M74dZDek=~1',
    'ak_bmsc': 'CEA9DE97850DA65FA3EEDD5BF18B00AE~000000000000000000000000000000~YAAQnegyF4MrFBqLAQAAp8tvHRXCf02rOE2Kuq1TrGtE6XkonGvvylEJp7tMlyYeryti6en67SHRPeeEQ8wp7xiv4g8PY2cmrtqVC5FM/DUNfTnMSmvysHRb8HHwUyzmJc+vjVtCcY0H7ZxO+/RZtRy5mIxqklIrbHpD6Zd+OpYE1DBMsiha70sFAQSShM9fvbdbmrfzb6U4eaeXwWM2W4rQtN9Je9uKVA/nf9UL8pcB2LccS7k/T3d6tevlzil4ZIm5PUvCiAEZPvzPCIwPwlzyce2CZNvSP2XLVu6p9sKvWhEy6JLFH/k/q9DjzHxzozWL0eGJJzffPGNSvWZq4irQELpHDXBS7uVo4QN+SAmM0KWOPlbaLBVnD4Nv94caR9G7/YvTawikx5EftnDnIlxQFdgygzVGdp3+7IJFOZlIu3x3bZV0/Atdhfv1klHpDaC32DXO4jjyPUBuSdDBifgaaFl2nt22GF4hNhR7EVJbKEEIvlEvV4AqtoVhQbtGnWX+6B5Ca9mKUZ4twumB6AS0lr8wcNvW7ys90eqvJKMX+tFwtX3A3p35hMzU7o7/Rog9dmTe2PNEbB+N9Oyvrzrn',
    '_dc_gtm_UA-31346-1': '1',
    '_gat_UA-735942-8': '1',
    'ln_or': 'eyIzOTk4NjE3IjoiZCJ9',
    '.AspNet.PendingCookies': 'sYsLzE3bc-XFWMusmI8DfukWj6HLmHyRswz3HEy6-M8mmQKqh8Asdn-0mPpIQBv2hu3gvDOU3dMx2ficZH-Sm-C7A6uTV_PnSKJ37WmHXT_Qqs6KLRJmMoHNe7EOdh1Sr_wvAIHl0INnZbxwXxtyq6pHXJU3hKpVy29Qq5cVdf54Rdhfm18oVtWD3_owXjXH7f0KXVZeLm6lDE9M1qoB7Ik0bod4wQe5hk-n-OcRSOTYtC9N0svfD6w4ik2VAsi7SbIp48foRO-_eMABL_FColFBaEekX98GO-TnHKx19dzq3DE-DW7pZqzsMnvNVKH1qccpmtGx4BTwu2f0LUX4zvW0CN4',
    '_uetsid': '32cf7a80673711ee8c8e47b1b8addd50',
    '_uetvid': '32cf9f70673711ee958c434077a4d1bb',
    '_ga': 'GA1.2.1636933398.1696919727',
    '_ga_2DVXTE8M0Q': 'GS1.1.1697005730.4.1.1697007216.29.0.0',
    '_ga_T35BRJ98N6': 'GS1.1.1697005730.4.1.1697007216.0.0.0',
    'RT': '"z=1&dm=www.loopnet.com&si=7948d0b6-5f3a-48c5-8612-3c18bca34b9d&ss=lnldhhpg&sl=4&tt=7rs&bcn=%2F%2F684d0d46.akstat.io%2F&ld=sl2y&nu=2b9r84y4&cl=snja"',
}

headers = {
    'authority': 'www.loopnet.com',
    'accept': 'application/json, text/plain, */*',
    'accept-language': 'en-US,en;q=0.9,id;q=0.8',
    'authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiI1Y2VlYWEwZS1mNDIzLTQyNzgtYjg3Ny0wNzNiYmU3YmMyZWYiLCJqdGkiOiI5YjFiMjUwNS1jNzQ0LTQzODctOTUzNC1jNjk5YzcyYzgzOTAiLCJlbnMiOiI0MCIsInN1aWQiOiIiLCJiZnNyIjoiMCIsImRpZCI6IjEwIiwidWFpZCI6IjQxNzYwOCIsInVpcCI6IjE3Mi4xOS4yOS45MyIsImV4cCI6MTY5NzAxMzIyNiwiaXNzIjoiaHR0cHM6Ly9hcGkuYml6YnV5c2VsbC5jb20iLCJhdWQiOiJsb29wbmV0In0.XqOSzQcK8JskMPeZLz0nIuqVfwqsXSzpWeAMSqWXK7I',
    'content-type': 'application/json',
    # 'cookie': 'LNUniqueVisitor=66fa2657-e069-4581-806e-955af663489c; _gcl_au=1.1.353722489.1696919725; _gid=GA1.2.474567762.1696919727; visitor_id923983=1179808731; visitor_id923983-hash=5c4bec8c28be7cac40155c5e07eee06f698962755bf8a186e3e73b62aafe97d6fe1b610b776e27c0613e14be2a50f67ab58d6ad8; SessionFarm_GUID=f0c5e7a6-d120-4241-91e0-78641ff45512; __RequestVerificationToken=HZ75vg0LBDCzlp9_DXSVgNsErPtgfgIsb7J_57YnoQMrLldUPPWcuJA6oxsLFoZTUENlAjnFtr9gBpyVFGiP_d6IqWQ1; lnwbprddc=04; cr_gum2=C_DYOdCsyW9h7L3EbzB_iTtyp6Z25zDe; g_state={"i_p":1697077953664,"i_l":2}; LastSearchPropertyType=%7B%22id%22%3A9001%2C%22pname%22%3A%22Office%22%2C%22lid%22%3Anull%2C%22pid%22%3A8%2C%22sid%22%3A0%2C%22scid%22%3A0%2C%22name%22%3A%22Office%22%2C%22namel%22%3A%22office%22%2C%22isforsale%22%3Afalse%2C%22isbbs%22%3Afalse%2C%22notfound%22%3Atrue%2C%22url%22%3Anull%7D; LastSearchLocation=%7b%22Typeahead%22%3a%22United+States%22%2c%22Geography%22%3a%7b%22ID%22%3a%221990%22%2c%22Display%22%3a%22United+States%22%2c%22GeographyType%22%3a15%2c%22Address%22%3a%7b%22CountryCode%22%3a%22US%22%2c%22ListingGeoCoded%22%3afalse%2c%22Country%22%3a%22US%22%2c%22CountryName%22%3a%22United+States%22%7d%2c%22Location%22%3a%7b%22y%22%3a45.176882%2c%22x%22%3a-123.2829875%7d%2c%22BoundingBox%22%3a%7b%22LowerRight%22%3a%7b%22y%22%3a18.9249%2c%22x%22%3a-66.940119%7d%2c%22UpperLeft%22%3a%7b%22y%22%3a71.428864%2c%22x%22%3a-179.625856%7d%7d%2c%22MatchType%22%3a15%2c%22SelectedCurrencyType%22%3a0%2c%22SelectedMeasurementConversion%22%3a0%2c%22DisplayCountryCode%22%3a%22USA%22%2c%22TypeaheadUIDisplay%22%3a%22United+States+-+USA%22%7d%2c%22Culture%22%3a%22en-US%22%7d; MeasurementConversion=2; numberOfSearches=2; SearchesSinceLastIntercept=2; UOMFromSRP=%7b%22LeaseRatePerSizeUom%22%3a0%2c%22UseDefaultLeaseRateUomAndTerm%22%3atrue%2c%22LeaseRateTerm%22%3a0%2c%22SpaceAvailableUom%22%3a2%2c%22BuildingSizeUom%22%3a1%2c%22LotSizeUom%22%3a1%7d; cto_bundle=LlRTzF92WVVleU5MNkY2SEFCOERxeXBLZkdPT3dZdCUyQnU2dmhYNWdiSUVEYklLM3pJRzA0TlJDbnhlYWZka05qRTdMSUZGUGt1bldpN1dHY2M3QUZmU0pkN05LMEZJNG0lMkJzZU9Wc1RZS1p6aEc1MEFQbSUyRlZsYTlMeUFUNmtsaHJDMVdxYjRkWnNhTHJrZW1oeWN6ZGU2QWlSWFJ0MHNTRUVwMEp4UFFaaTYwQk52UFdqdnlzaFhKSnFMdTRGTkhLMXZkYjhZcHdLJTJGdTJkcjNKelduaTB6NHolMkZ1R0RsRFBiTWpjdkRwODlRemVzcHRWZ0cyZllNanp4Q1YwN1BjaEJxdE9uelhsTnRYcDVocFVvJTJGOUZSUTVGbE9oVW56U0hlYTNVQmRoQWRCeE4lMkJnbUs0QUhNM1BXanpsb3hUZ3J3S0dMZ2RK; gip=%7b%22Display%22%3a%22United+States%22%2c%22GeographyType%22%3a15%2c%22Address%22%3a%7b%22City%22%3a%22San+Francisco%22%2c%22PostalCode%22%3a%2294111%22%2c%22State%22%3a%22CA%22%2c%22CountryCode%22%3a%22US%22%2c%22Location%22%3a%7b%22y%22%3a37.7749295%2c%22x%22%3a-122.4194155%7d%2c%22ListingGeoCoded%22%3afalse%2c%22Country%22%3a%22US%22%7d%2c%22Location%22%3a%7b%22y%22%3a-6.13%2c%22x%22%3a106.75%7d%2c%22MatchType%22%3a0%2c%22SelectedCurrencyType%22%3a0%2c%22SelectedMeasurementConversion%22%3a0%2c%22lc%22%3a%22en-US%22%2c%22cy%22%3a%22USD%22%2c%22tgeo%22%3a%7b%22cc%22%3a%22ID%22%2c%22sc%22%3a%2204%22%2c%22ct%22%3a%22Jakarta%22%2c%22pc%22%3a%2211430%22%2c%22ly%22%3a-6.13%2c%22lx%22%3a106.75%2c%22lc%22%3a%22en-US%22%2c%22cy%22%3a%22USD%22%7d%7d; bm_mi=103395C52F5C47400BCF586D5A4FDFE2~YAAQnegyF0ElFBqLAQAA/chuHRXJ6B2yqjhj9n9WE1ggUS1/hstnwDIOY6f8oJ2RgN98Ae4MNcooNjvwfFZdHrkww6WQQBLDELV/ZVEkoezOGNflAAdAhr8WVGFrwgsVUWbmrIbHt62zyV7/DUvQMJobP/H8Oe+vOTT3v8zWrKFiFjy/bvtmN4JkBIOieqfVj/TQPmgQtIlUnYtzyAnN01T8a2PmpOS0t9uM/mgUCF0DUtFLpDXF+NOEN0TZaS2MeUfGDAWODuO5xI+FWFO8CLMzwa6kC+T59he7o9hNbdQxLFRM/q1fPs/A0OvZxH3vFnDBiRZttHKwq6OW2cOa1TxdWZ7i07Obk+Z/NbQ+Lbtrf8ndF/HdV4nPHA==~1; bm_sv=A8495C66B5DB131BE201953AD2024F6B~YAAQnegyF58lFBqLAQAAIdJuHRXjwQ3MOQF0aKcEtaBixoATBo+W8K/fv/MjvUbxCGqm9HSx/gvMK3vxt6fB+h0jZm9ZuT2whLg8N3K0X2o4w1NBm+bxZqxS4z16L6OUWM95Bn8g9RNHjQqL+jP3qQTrcAEOs6tDyu4KVZBjyTow8r9S87I6dSS52Qc7egd7KRJ1SOF14uON506mb0fanthMBdet690CGKA/D2vjymZw3dUl9kwV8mP7L90M74dZDek=~1; ak_bmsc=CEA9DE97850DA65FA3EEDD5BF18B00AE~000000000000000000000000000000~YAAQnegyF4MrFBqLAQAAp8tvHRXCf02rOE2Kuq1TrGtE6XkonGvvylEJp7tMlyYeryti6en67SHRPeeEQ8wp7xiv4g8PY2cmrtqVC5FM/DUNfTnMSmvysHRb8HHwUyzmJc+vjVtCcY0H7ZxO+/RZtRy5mIxqklIrbHpD6Zd+OpYE1DBMsiha70sFAQSShM9fvbdbmrfzb6U4eaeXwWM2W4rQtN9Je9uKVA/nf9UL8pcB2LccS7k/T3d6tevlzil4ZIm5PUvCiAEZPvzPCIwPwlzyce2CZNvSP2XLVu6p9sKvWhEy6JLFH/k/q9DjzHxzozWL0eGJJzffPGNSvWZq4irQELpHDXBS7uVo4QN+SAmM0KWOPlbaLBVnD4Nv94caR9G7/YvTawikx5EftnDnIlxQFdgygzVGdp3+7IJFOZlIu3x3bZV0/Atdhfv1klHpDaC32DXO4jjyPUBuSdDBifgaaFl2nt22GF4hNhR7EVJbKEEIvlEvV4AqtoVhQbtGnWX+6B5Ca9mKUZ4twumB6AS0lr8wcNvW7ys90eqvJKMX+tFwtX3A3p35hMzU7o7/Rog9dmTe2PNEbB+N9Oyvrzrn; _dc_gtm_UA-31346-1=1; _gat_UA-735942-8=1; ln_or=eyIzOTk4NjE3IjoiZCJ9; .AspNet.PendingCookies=sYsLzE3bc-XFWMusmI8DfukWj6HLmHyRswz3HEy6-M8mmQKqh8Asdn-0mPpIQBv2hu3gvDOU3dMx2ficZH-Sm-C7A6uTV_PnSKJ37WmHXT_Qqs6KLRJmMoHNe7EOdh1Sr_wvAIHl0INnZbxwXxtyq6pHXJU3hKpVy29Qq5cVdf54Rdhfm18oVtWD3_owXjXH7f0KXVZeLm6lDE9M1qoB7Ik0bod4wQe5hk-n-OcRSOTYtC9N0svfD6w4ik2VAsi7SbIp48foRO-_eMABL_FColFBaEekX98GO-TnHKx19dzq3DE-DW7pZqzsMnvNVKH1qccpmtGx4BTwu2f0LUX4zvW0CN4; _uetsid=32cf7a80673711ee8c8e47b1b8addd50; _uetvid=32cf9f70673711ee958c434077a4d1bb; _ga=GA1.2.1636933398.1696919727; _ga_2DVXTE8M0Q=GS1.1.1697005730.4.1.1697007216.29.0.0; _ga_T35BRJ98N6=GS1.1.1697005730.4.1.1697007216.0.0.0; RT="z=1&dm=www.loopnet.com&si=7948d0b6-5f3a-48c5-8612-3c18bca34b9d&ss=lnldhhpg&sl=4&tt=7rs&bcn=%2F%2F684d0d46.akstat.io%2F&ld=sl2y&nu=2b9r84y4&cl=snja"',
    'origin': 'https://www.loopnet.com',
    'referer': 'https://www.loopnet.com/biz/businesses-for-sale/',
    'requestverificationtoken': 'BTzxwftrTa0XAh3bQYXora5uYyuBHMVbKj3YxrA5pYLz2NfpFwRc_U0ksUx26EhSLUj82LxeXXkodIImN-okKvj0b4c1',
    'sec-ch-ua': '"Google Chrome";v="117", "Not;A=Brand";v="8", "Chromium";v="117"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Windows"',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-origin',
    'traceparent': '00-00000000000000004ccd1c2f94896878-dc6816ec8972a6b0-01',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36',
    'x-correlation-id': '',
}

datas = []

for page in range(1, 278):

    json_data = {
    'bfsSearchCriteria': {
        'siteId': 40,
        'languageId': 10,
        'categories': None,
        'locations': None,
        'askingPriceMax': 0,
        'askingPriceMin': 0,
        'pageNumber': page,
        'keyword': None,
        'cashFlowMin': 0,
        'cashFlowMax': 0,
        'grossIncomeMin': 0,
        'grossIncomeMax': 0,
        'daysListedAgo': 0,
        'establishedAfterYear': 0,
        'listingsWithNoAskingPrice': 0,
        'listingsWithSellerFinancing': 0,
        'realEstateIncluded': 0,
        'showRelocatableListings': False,
        'relatedFranchises': 0,
    },
    'industriesHierarchy': 10,
    'languageTypeId': 10,
    'email': '',
}

    response = requests.post(
        'https://www.loopnet.com/biz/services/bff/v2/LoopNetBfsSearchResults',
        cookies=cookies,
        headers=headers,
        json=json_data,
    )

    if response.status_code == 200:
        datas.append(response.json())
        print(page)
    
with open(f'D:\\freelance\\restaurant-bussiness-scrapping\\data.json', 'a') as file:
    json.dump(datas, file)
        
